class Comment
  include Mongoid::Document
  field :body, type: String
  field :upvotes, type: Integer
  embedded_in :post
  embedded_in :user
end
