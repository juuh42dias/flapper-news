class Post
  include Mongoid::Document
  field :title, type: String
  field :link, type: String
  field :upvotes, type: Integer
  embedded_in :user
end
